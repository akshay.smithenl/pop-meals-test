package org.akshay;

public class OptimalBreakTime {

    public static void main(String[] args) {
        int[] riderRoute = {1, 4, 1, 3, 1};
        System.out.print("Rider Route: {");
        for (int i:riderRoute) {
            System.out.print(" " + i + ",");
        }
        System.out.println("}");
        int optimalBreakTime = findOptimalBreakTime(riderRoute, riderRoute.length);
        System.out.println( (optimalBreakTime==-1 || optimalBreakTime==riderRoute.length) ?
                "No Optimal Break Time for the Rider. :-(" : "Optimal Break Time for the rider is at : " + optimalBreakTime);
    }

    /**
     * Find the Optimal break time for the rider if its available
     * @param array
     * @param length
     * @return
     */
    public static int findOptimalBreakTime(int[] array, int length) {
        int left = 0;
        int right = 0;
        int optimalBreak = -1;

        //Traversing the whole route
        for (int i = 0; i < length; i++) {
            //Sum of left
            left +=array[i];

            //Traversing the right
            right = 0;
            for (int j=i+1; j<length; j++) {
                //Sum of right
                right +=array[j];
            }

            if(left == right) {
                //optimal Break found, exit the loop
                optimalBreak = i;
                break;
            }
        }
        return optimalBreak;
    }
}
