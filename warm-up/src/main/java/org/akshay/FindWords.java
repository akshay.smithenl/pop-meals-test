package org.akshay;

import java.util.stream.IntStream;

public class FindWords {
    /**
     * ASSUMPTION: There are no special characters in the letters
     * @param args
     */
    public static void main(String[] args) {
        String letters = "ProgramToFindTheWordsInAString";
        System.out.println("Letters: " + letters + ", words in it: " + findNumberOfWords(letters));

        //One line solution
        IntStream intStream = letters.chars();
        System.out.println("OneLiner: Letters:" + letters + ", words in it: "
                + intStream.filter(ch -> ch>=65 && ch<=90).count());
    }

    /**
     * ASSUMPTION: There are no special characters in the stringOfLetters
     *
     * @param stringOfLetters
     * @return number of words in the strings
     */
    public static int findNumberOfWords(String stringOfLetters) {
        int words = 0;
        char[] charArray = stringOfLetters.toCharArray();
        for (int ch : charArray) {
            //Uppercase letter
            if(ch>=65 && ch<=90) {
                words++;
            }
        }
        return words;
    }
}