# pop-meals-test



## Pop Meals Full-Stack Coding Challenge

Trying to solve three problems.<br/>

## Problem 1 - Warm Up

Suppose you are given a string without spaces containing multiple words in camel-case. Write a method
returning the number of words in that string.

*Bonus*: Use a stream (Java) to achieve this task as a one-liner.
Example:
````
Input "thisContainsFourWords" -> Output 4
````
###
**Program**: [FindWords](#org.akshay.FindWords)
***************************
## Problem 2 - Optimal Break Time

Our delivery riders need to complete longer and longer routes. They need a break! Find the optimal break
for our rider according to the specs below:
You are given an array of integers of length n for a rider. The integer at index `i` symbolizes the time
allocated to her or his `i'th` task in minutes. The optimal time for a break is between two tasks `i`
and `i+1`, when the sum of all tasks from 0 to i equals the sum `i+1` to `n`.

Write a method that returns index i where the condition above is met. If there is no such index the rider
is not awarded a break and we return null. Consider the runtime complexity of your solution!
Bonus: Can you find a solution that efficiently handles 3, 4, …, m breaks?
Example:
````
Input [1, 4, 1, 3, 1] -> Output 1
````
###
**Program**: [OptimalBreakTime](#org.akshay.OptimalBreakTime)
****************************
## Problem 3 - Insertion Routing

During lunch time Kuala Lumpur is flooded with Pop Meals riders. Each of these riders has a pre-defined
route they are completing. A route is an ordered list of deliveries, that have to be completed in the given
order.
We want to leverage this by allowing customers to place orders after the riders started their routes. To
achieve this we have to insert deliveries in existing routes.
Goal:
Think of a heuristic that given a delivery to insert and a list of routes returns an insertion point (route and
index), which minimizes the additional time the rider needs to spend on the road.
![image_ref](warm-up/src/main/resources/route-insertion.JPG)
**Assumptions**:
* You have access to a class that implements the interface ``DistanceService``, which you can use
to get the time required to get from delivery to delivery
* Riders have enough food with them to deliver as many extra customers as needed<br/>
**Objective**:
Implement interface InsertionService according to the comments on the methods.
Bonus: Assume DistanceService has an additional method int
``getDistanceInMeters(Delivery a, Delivery b)``. Can you devise another implementation of
InsertionService that tries to minimize both additional time and distance assuming you get 2
additional parameters a and b as weights for time and distance?
###
**Program**: [InsertionRoutingService](org.akshay.InsertionRoutingService)
****************************
